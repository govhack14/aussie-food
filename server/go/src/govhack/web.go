package main

import(
	"log"
	"net/http"
	"os"
    "fmt"
)

func main(){
	port := os.Getenv("GOVHACK_WEB_PORT")
	if port == ""{
		port = "8080"
	}
    dir := os.Getenv("GOVHACK_STATIC_WEB")
    fmt.Printf("Serving %s at port %s\n", dir, port)
	log.Fatal(http.ListenAndServe(":" + port, http.FileServer(http.Dir(dir))))
}
