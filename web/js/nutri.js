
$(document).ready(function () {

    var nutri_provider_url = "/data"; // x-site call not allowed
    $('.product').each( function(){
        var el = $(this);
        var id = $(this).attr('id');

        var jqxhr = $.getJSON(nutri_provider_url + "/" + id + ".json",


      function (res){
          $(".badge.protein", el).text( format( res['protein_g'] ));
          $(".badge.fat", el).text(format( res['total_fat_g']) );
          $(".badge.carbohydrate", el).text( format( res['available_carbohydrates_with_sugar_alcohols_g']) );
      });
    });

    function format(number){
        return number.toFixed(0) + '%';
    }
});
