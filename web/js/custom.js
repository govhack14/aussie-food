// opening picture fading
$(window).bind('scroll', function(){
    var fadeStart=50 // 100px scroll or less will equiv to 1 opacity
        ,fadeUntil=400 // 200px scroll or more will equiv to 0 opacity
        ,fading = $('.openPic');
    var offset = $(document).scrollTop()
        ,opacity=0;
    if( offset<=fadeStart ){
        opacity=1;
    }else if( offset<=fadeUntil ){
        opacity=1-offset/fadeUntil;
    }
    fading.css('opacity',opacity).html(opacity);
});

// pie chart
var dataSet = [
    {label: "Asia", data: 4119630000, color: "#005CDE" },
    { label: "Latin America", data: 590950000, color: "#00A36A" },
    { label: "Africa", data: 1012960000, color: "#7D0096" },
    { label: "Oceania", data: 35100000, color: "#992B00" },
    { label: "Europe", data: 727080000, color: "#DE000F" },
    { label: "North America", data: 344120000, color: "#ED7B00" }    
];

var options2 = {
    series: {
        pie: {
            show: true,
            innerRadius: 0.5,
            label: {
                show: true
            }
        }
        
    },
    legend:{
        show: false
    }
};

// $.plot($(".col-sm-3a"), dataSet, options2);

// time series
var data6 = [];
 
function DoSeries6(){
    var data = [];
    var start = 1354586000000;
 
    for(i=1;i<=50;i++){                
        data.push([start, i * Math.cos(i)]);
        start+= 100000
    }
 
    return data;
}

var data7 = [];
 
function DoSeries7(){
    var data = [];
    var start = 1354586000000;
 
    for(i=1;i<=50;i++){                
        data.push([start, i * Math.sin(i)]);
        start+= 100000
    }
 
    return data;
}


// ajax
var cpu = [], cpuCore = [], disk = [];
var dataset;
var totalPoints = 100;
var updateInterval = 5000;
var now = new Date().getTime();

var options = {
    series: {
        lines: {
            lineWidth: 1.2
        },
        bars: {
            align: "center",
            fillColor: { colors: [{ opacity: 1 }, { opacity: 1}] },
            barWidth: 500,
            lineWidth: 1
        }
    },
    xaxis: {
        mode: "time",
        tickSize: [60, "second"],
        tickFormatter: function (v, axis) {
            var date = new Date(v);

            if (date.getSeconds() % 20 == 0) {
                var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                return hours + ":" + minutes + ":" + seconds;
            } else {
                return "";
            }
        },
        axisLabel: "Time",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10
    },
    yaxes: [
        {
            min: 0,
            max: 100,
            tickSize: 5,
            tickFormatter: function (v, axis) {
                if (v % 10 == 0) {
                    return v + "%";
                } else {
                    return "";
                }
            },
            axisLabel: "CPU loading",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 6
        }, {
            max: 5120,
            position: "right",
            axisLabel: "Disk",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 6
        }
    ],
    legend: {
        noColumns: 0,
        position:"nw"
    },
    grid: {      
        backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
    }
};

function initData() {
    for (var i = 0; i < totalPoints; i++) {
        var temp = [now += updateInterval, 0];

        cpu.push(temp);
        cpuCore.push(temp);
        disk.push(temp);
    }
}

function GetData() {
    $.ajaxSetup({ cache: false });

    $.ajax({
        url: "http://www.jqueryflottutorial.com/AjaxUpdateChart.aspx",
        dataType: 'json',
        success: update,
        error: function () {
            setTimeout(GetData, updateInterval);
        }
    });
}

var temp;

function update(_data) {
    cpu.shift();
    cpuCore.shift();
    disk.shift();

    now += updateInterval

    temp = [now, _data.cpu];
    cpu.push(temp);

    temp = [now, _data.core];
    cpuCore.push(temp);

    temp = [now, _data.disk];
    disk.push(temp);

    dataset = [
        { label: "CPU:" + _data.cpu + "%", data: cpu, lines: { fill: true, lineWidth: 1.2 }, color: "#00FF00" },
        { label: "Disk:" + _data.disk + "KB", data: disk, color: "#0044FF", bars: { show: true }, yaxis: 2 },
        { label: "CPU Core:" + _data.core + "%", data: cpuCore, lines: { lineWidth: 1.2}, color: "#FF0000" }        
    ];

    $.plot(".col-sm-3c", dataset, options);
    setTimeout(GetData, updateInterval);
}

// epoch getting started
var dataE = [
    {
    label: "Layer 1",
    values: [ {x: 0, y: 100}, {x: 20, y: 1000}],
  },

  // The second layer
  {
    label: "Layer 2",
    values: [ {x: 0, y: 78}, {x: 20, y: 98}]
  },
];

// epoch bar real-time
var barChartData = [
  // First series
  {
    label: "Series 1",
    values: [{time: 1370044800, y: 78}, {time: 1370044801, y: 98}]
  }
];

$(document).ready(function () {
    
    // pie chart
    $.plot($(".col-sm-3a"), dataSet, options2);

    // time series
    data6 = DoSeries6();

    var options = {
        xaxis: {
            mode: "time"
        },
        grid:{
            // backgroundColor: { colors: ["#969696", "#5C5C5C"] }
        }
    };

    $.plot($(".col-sm-3b"),
    [
        {
          data: data6,
          points: { show: true },
          lines: { show: true}
        }
    ],
    options);

    // ajax
    initData();

    dataset = [        
        { label: "CPU", data: cpu, lines:{fill:true, lineWidth:1.2}, color: "#00FF00" },
        { label: "Disk:", data: disk, color: "#0044FF", bars: { show: true }, yaxis: 2 },
        { label: "CPU Core", data: cpuCore, lines: { lineWidth: 1.2}, color: "#FF0000" }
    ];

    $.plot(".col-sm-3c", dataset, options);
    setTimeout(GetData, updateInterval);

    // user activated plot
    var optionsA = {
        lines: {
            show: true
        },
        points: {
            show: true
        },
        xaxis: {
            // tickDecimals: 0,
            // tickSize: 1
        }
    };


    $("#btn").click(function () {

        data7 = DoSeries7();

        var res = [{
            label: "data1",
            data: data6,
            lines: {
                show: true
            },
            points:{
                show:true
            }
            },{
            label: "data2",
            data: data7,
            lines: {
                show: true
            },
            points:{
                show:true
            }
        }];

        $.plot($(".col-sm-3b"),res,options);
   
    });

    // epoch getting started
    var areaChartInstance = $('#area').epoch({ type: 'area', data: dataE, width: 500});

    // // epoch bar real-time
    // $('#area2').epoch({
    //     type: 'time.bar',
    //     data: barChartData
    // });

});



// $(document).ready(function () {

    // ajax user activated plot
    // var optionsA = {
    //     lines: {
    //         show: true
    //     },
    //     points: {
    //         show: true
    //     },
    //     xaxis: {
    //         // tickDecimals: 0,
    //         // tickSize: 1
    //     }
    // };


    // $.plot($(".col-sm-3d"), dataA, optionsA);

    // // Fetch one series, adding to what we already have

    // var alreadyFetched = {};

    // $("#btn").click(function () {

    //     var button = $(this);

    //     // Find the URL in the link right next to us, then fetch the data

    //     //var dataurl = button.siblings("a").attr("href");

    //     function onDataReceived(series) {

    //         // Extract the first coordinate pair; jQuery has parsed it, so
    //         // the data is now just an ordinary JavaScript object

    //             var firstcoordinate = "(" + series.data[0][0] + ", " + series.data[0][1] + ")";
    //             button.siblings("span").text("Fetched " + series.label + ", first point: " + firstcoordinate);

    //             // Push the new data onto our existing data array

    //             if (!alreadyFetched[series.label]) {
    //                 alreadyFetched[series.label] = true;
    //                 dataA.push(series);
    //             }
    //             // console.log(dataA[0]);
    //             // console.log(optionsA);
    //             $.plot($("col-sm-3d"), dataA, optionsA);
    //     } 

    //     $.ajax({
    //         url: "http://www.flotcharts.org/flot/examples/ajax/data-eu-gdp-growth.json",
    //         type: "GET",
    //         dataType: "json",
    //         success: onDataReceived
    //     });
    // });
    // $("#btn").click();

// });



// // Initiate a recurring data update

// $("#btn btn-default").click(function () {

//     data = [];
//     alreadyFetched = {};

//     $.plot(".col-sm-3c", data, options);

//     var iteration = 0;

//     function fetchData() {

//         ++iteration;

//         function onDataReceived(series) {

//             // Load all the data in one pass; if we only got partial
//             // data we could merge it with what we already have.

//             data = [ series ];
//             $.plot(".col-sm-3c", data, options);
//         }

//         // Normally we call the same URL - a script connected to a
//         // database - but in this case we only have static example
//         // files, so we need to modify the URL.

//         $.ajax({
//             url: "data-eu-gdp-growth-" + iteration + ".json",
//             type: "GET",
//             dataType: "json",
//             success: onDataReceived
//         });

//         if (iteration < 5) {
//             setTimeout(fetchData, 1000);
//         } else {
//             data = [];
//             alreadyFetched = {};
//         }
//     }

//     setTimeout(fetchData, 1000);
// });

// Load the first series by default, so we don't have an empty plot
 
 