buttonClicked = 0;
itemlist = [];

$(document).ready(function () {

    $("#meat").click(function () {
        $("#all").click();
        $(".product").each(function(){
            $(".product").hide();
        })
        $(".meat").show();
        $("p.lead").text('Meat');

        // $("p.lead").text('Meat');

    	// var elems = $(".menu-category-name").get();
    	// //console.log(elems);
    	// //console.log(elems.length);
    	// for (i=0;i<elems.length;i++){

    	// 	if ($(elems[i]).text() !== 'Meat'){
    	// 		// //console.log($(elems[i]).text());

    	// 		// //console.log($(elems[i]).parent());
    	// 		$(elems[i]).parent().hide();
    	// 	}
    	// }

    });

     $("#veg").click(function () {
        $("#all").click();
        $(".product").each(function(){
            $(".product").hide();
        })
        $(".veg").show();
        $("p.lead").text('Vegetable');
    });

     $("#confec").click(function () {
        $("#all").click();
        $(".product").each(function(){
            $(".product").hide();
        })
        $(".confec").show();
        $("p.lead").text('Confectionary');
    });

     $("#bev").click(function () {
        $("#all").click();
        $(".product").each(function(){
            $(".product").hide();
        })
        $(".bev").show();
        $("p.lead").text('Beverage');
    });

    $("#staples").click(function () {
        $("#all").click();
        $(".product").each(function(){
            $(".product").hide();
        })
        $(".staple").show();
        $("p.lead").text('Staple');
    });

   	$("#fruits").click(function () {
        $("#all").click();
        $(".product").each(function(){
            $(".product").hide();
        })
        $(".fruit").show();
        $("p.lead").text('Fruit');

    });

   	$("#dairy").click(function () {
        $("#all").click();
        $(".product").each(function(){
            $(".product").hide();
        })
        $(".dairy").show();
        $("p.lead").text('Dairy');
    });

    $("#all").click(function () {
        $(".product").each(function(){
            $(".product").show();
        })
        $("p.lead").text('Everything');
    });


    $(".product").find(".btn").click(function(){

        buttonClicked = 1;

        var prod_name = $(this).siblings('.menu-category-name').text();
        var prod_id = $(this).parent().attr('id');

        var prod_key = prod_name + "#" + prod_id;
        //console.log(prod_key);
        // Retrieve the object from storage
        // var val = JSON.parse(localStorage.getItem('fooditem'));
        var val = sessionStorage.getItem(prod_key);
        if (val!== null){
                var x = Number(val);
                x++;
                sessionStorage.setItem(prod_key,x);
        }
        else{
            sessionStorage.setItem(prod_key,1);
        }

        /**
         * when button is clicked we fade to 10% opacity
         * and after 100 ms we fade the box back to 100%
         */
        $(this).fadeTo(100, 0.1).fadeTo(200, 1.0);


    });

    $(".product").click(function(){


        if (buttonClicked === 0){
            $("#modal-body-col1").empty();
            $("#modal-body-col2").empty();
            $("#modal-body-col3").empty();
            $("#modal-body-col4").empty();

            var id = $(this).attr('id');
            var x = $(this).find($("img")).attr("src");
            var prod_name = $(this).find($(".menu-category-name")).text();
            // //console.log(x);
            // $('#myModal').modal();
            $("#myModalLabel").text(prod_name);
            $("#modal-body-col1").append('<img class="modal_img" src='+'"'+x+'"'+ 'width="100px">');
            // var advice = "Descriptive Information";
            $("#modal-body-col1").append('<div style="height:30px"> </div>');
            $("#modal-body-col1").append('<div class="font tstyle">'+advice(id)+'</div>');

            $("#modal-body-col2").append('<div id="col2_chart" class="epoch category30"  style="width: 200px; height: 400px"></div>');

            var nutri_provider_url = "/data"; // x-site call not allowed

            // var jqxhr = $.getJSON(nutri_provider_url + "/" + id + ".json",
            var jqxhr = $.getJSON(nutri_provider_url + "/" + id + ".json",

          //if(res != undefined) {

                function (res){
                  //console.log('hello');

                    var barChartData = [
                    {
                        label: 'Product',
                        values: [
                          { x: 'Protein', y: res['protein_g']/50 },
                          { x: 'Fat', y: res['total_fat_g']/70 },
                          { x: 'Carbs', y: res['available_carbohydrates_with_sugar_alcohols_g']/300 }
                        ]
                    },

                    {
                        label: 'RDI',
                        values: [
                          { x: 'Protein', y: 1 },
                          { x: 'Fat', y: 1 },
                          { x: 'Carbs', y: 1 }
                        ]
                    }
                    ];

                    var areaChartInstance=$('#col2_chart').epoch({
                        type: 'bar',
                        data: barChartData
                    });

                    $("#modal-body-col3").append('<div id="col3_chart" style="width: 500px; height: 400px"></div>');
                                //Width and height
                    var w = 400;
                    var h = 300;
                    var barPadding = 5;
                                //Original data
                    var svg = d3.select("#col3_chart").append("svg:svg")
                                .attr("class", "chart")
                                .attr("width", w)
                                .attr("height", h)
                                .append("svg:g")
                                .attr("transform", "translate(0,375)");


                                x = d3.scale.ordinal().domain(["VitB6", " VitB12", " VitC", "Iodine","Iron","VitE", "Cal", "Mg", "Phos","Selenium"]).rangeRoundBands([0, w]);
                                // x = d3.scale.ordinal().rangeRoundBands([0, w-20])
                                y = d3.scale.linear().range([0, h-10])
                                z = d3.scale.ordinal().range(["green", "lightblue"])

                                ////console.log("RAW MATRIX---------------------------");
                            // 4 columns: ID,c1,c2,c3
                                var matrix = [
                                    [ 1,  res['vitamin_b6_mg']/1.1, 1-res['vitamin_b6_mg']/1.1],
                                    [ 2, res['vitamin_b12__ug']/2, 1-res['vitamin_b12__ug']/2],
                                    [ 3, res['vitamin_c_mg']/30, 1-res['vitamin_c_mg']/30],
                                    [ 4,  res['iodine_i_ug']/100,  1-res['iodine_i_ug']/100],
                                    [ 6,   res['iron_fe_mg']/8,  1-res['iron_fe_mg']/8],
                                    [ 7,   res['vitamin_e_mg']/7,  1-res['vitamin_e_mg']/7],
                                    [ 8,   res['calcium_ca_mg']/1000,  1-res['calcium_ca_mg']/1000],
                                    [ 11,  res['magnesium_mg_mg']/310, 1-res['magnesium_mg_mg']/310],
                                    [ 9,  res['phosphorus_p_mg']/1000, 1-res['phosphorus_p_mg']/1000],
                                    [ 10, res['selenium_se_ug']/60, 1-res['selenium_se_ug']/60]

                                ];
                                ////console.log(matrix);

                                ////console.log("REMAP---------------------------");
                                var remapped =["c1","c2"].map(function(dat,i){
                                    return matrix.map(function(d,ii){
                                        return {x: ii, y: d[i+1] };
                                    })
                                });
                                //console.log(remapped)

                                //console.log("LAYOUT---------------------------");
                                var stacked = d3.layout.stack()(remapped)
                                //console.log(stacked)


                                x.domain(stacked[0].map(function(d) { return d.x; }));
                                y.domain([0, d3.max(stacked[stacked.length - 1], function(d) { return d.y0 + d.y; })]);

                                // show the domains of the scales
                                //console.log("x.domain(): " + x.domain())
                                //console.log("y.domain(): " + y.domain())
                                //console.log("------------------------------------------------------------------");

                                // Add a group for each column.
                                var valgroup = svg.selectAll("g.valgroup")
                                .data(stacked)
                                .enter().append("svg:g")
                                .attr("class", "valgroup")
                                .style("fill", function(d, i) { return z(i); })

                                // Add a rect for each date.
                                var rect = valgroup.selectAll("rect")
                                .data(function(d){return d;})
                                .enter().append("svg:rect")
                                .attr("x", function(d) { return x(d.x); })
                                .attr("y", function(d) { return -y(d.y0) - y(d.y); })
                                .attr("height", function(d) { return y(d.y); })
                                .attr("width", x.rangeBand()-barPadding);

                                x = d3.scale.ordinal().domain(["VitB6", " VitB12", " VitC", "Iodine","Iron","VitE", "Cal", "Mg", "Phos","Selenium"]).rangePoints([0, w]);

                                var xAxis = d3.svg.axis()
                                    .scale(x)
                                    .tickSize(5)
                                    .tickPadding(4)
                                    .orient("bottom");

                                svg.append("g")
                                    .attr("class", "axis")
                                    // .attr("transform", "translate(0," + height + ")")
                                    .call(xAxis);


        });
        // $("#modal-body-col4").append('<div id="col4_chart" style="width: 200px; height: 400px"></div>');

        $('#myModal').modal();
    }
    buttonClicked = 0;
});

    $(".mybasket").click(function(){

        // if (buttonClicked === 0){
            $("#modal-body-col1").empty();
            $("#modal-body-col2").empty();
            $("#modal-body-col3").empty();
            $("#modal-body-col4").empty();

            // Get storage in order to display to user
            $("#myModalLabel").text('My Basket');
            var x = "images/cart-icon.jpg";
            // $("#modal-body-col1").attr("class","nav_col2");
            $("#modal-body-col1").append('<img class="modal_img" src='+'"'+x+'"'+ 'width="130px">');
            $("#modal-body-col1").append('<h5 class="h5_grp"></h5>');

            var prods = [];
            for(var i=0; i<sessionStorage.length;i++){
                var prod_key = sessionStorage.key(i);
                var count = sessionStorage.getItem(prod_key);
                var prod_fields = prod_key.split("#");
                if(prod_fields.length > 1){
                    var prod_id = prod_fields[1];
                    var name = prod_fields[0];
                    ////console.log(prod_key + " " + name);
                    prods.push([prod_id, count]);
                    $("#modal-body-col1").append('<div class="font2">'+name+':  '+count+'</div>');
                }

            }

            $("#modal-body-col1").append('<div style="height: 300px"></div>');
            $("#modal-body-col1").append('<button class="btn btn-danger btn-sm rm_btn"><span class="glyphicon glyphicon-remove"></span> Remove all</button></img>');
            $("#modal-body-col3").append('<div id="col3_chart" class="epoch category40" style="width: 300px; height: 400px"></div>');
            $("#modal-body-col3").append('<div class="font2">'
                             + '<svg width="120" height="120">'
                             + ' <rect x="90" y="60" width="30" height="30" style="fill:rgb(31,119,180);stroke-width:1;stroke:rgb(0,0,0)" />'
                             + ' <text x="130" y="80">Basket</text>'
                             + ' <rect x="90" y="20" width="30" height="30" style="fill:rgb(255,127,14);stroke-width:1;stroke:rgb(0,0,0)" />'
                             + ' <text x="130" y="40">Recommended Dietary Intake</text>'
                             +'</svg>'
                             +'</div>');

            var nutriBasket = calcBasketNutrition(prods);
            var userRDI = calcUserRdi();


            var barChartData = [
            {
                label: 'Basket',
                values: [
                  { x: 'protein', y: nutriBasket['protein'] },
                  { x: 'carbohydrate', y:nutriBasket['carbohydrate']},
                  { x: 'fat', y: nutriBasket['fat'] }
                ]
            },

            {
                label: 'RDI',
                values: [
                  { x: 'protein', y: userRDI['protein'] },
                  { x: 'carbohydrate', y:userRDI['carbohydrate']},
                  { x: 'fat', y: userRDI['fat'] }
                ]
            }];

            // $("#modal-body-col2").html("<h4>Product name:"+ retrievedObject.name+ "</h4>");
            // $("#modal-body-col2").append("<h4>Quantity:"+ retrievedObject.count+ "</h4>");
            var areaChartInstance=$('#col3_chart').epoch({
                type: 'bar',
                data: barChartData
            });

        // }
            // $("#modal-body-col2").html("<h4>Product name:"+ retrievedObject.name+ "</h4>");
            // $("#modal-body-col2").append("<h4>Quantity:"+ retrievedObject.count+ "</h4>");
            $('#myModal').modal();
    });

    $("#myModal").on('click','.rm_btn', function(){
        sessionStorage.clear();
        $(".mybasket").click();
    });

    function calcBasketNutrition(basket){
        //.look in the contents of the basket and totalise the
        var res =[];

        if(basket.length > 0){
            res['protein'] = 55;
            res['carbohydrate'] = 35;
            res['fat'] = 10;
        }else{

            res['protein'] = 0;
            res['carbohydrate'] = 0;
            res['fat'] = 0;
        }

        return res;
    }

    function calcUserRdi(){


        var res = [];
        res['protein'] = 25;
        res['carbohydrate'] = 65;
        res['fat'] = 10;

        return res;
    }
});

// Inspired by Lee Byron's test data generator.
function bumpLayer(n, o) {

  function bump(a) {
    var x = 1 / (.1 + Math.random()),
        y = 2 * Math.random() - .5,
        z = 10 / (.1 + Math.random());
    for (var i = 0; i < n; i++) {
      var w = (i / n - y) * z;
      a[i] += x * Math.exp(-w * w);
    }
  }

  var a = [], i;
  for (i = 0; i < n; ++i) a[i] = o + o * Math.random();
  for (i = 0; i < 5; ++i) bump(a);
  return a.map(function(d, i) { return {x: i, y: Math.max(0, d)}; });
}

function format(number){
    return number.toFixed(0) + '%';
}

function advice(id){
var blurb="";
switch(id){
    case "chicken":
        blurb = "Although lean chicken meat is high in phsophorous, B6 and B12, commerically prepared, crumbed/fried chicken burgers can have many additives, sodium and differing percentages of actual checken flesh. ";
        break;
    case "cabbage":
        blurb = "Cruciferous vegetables (such as broccoli, cabbage, cauliflower, brussels sprouts and bok choy) have compounds that provide protection against some cancers.";
        break;
    case "beef":
        blurb = "Lean red meats are a particularly good source of iron, zinc and B12 and are easily absorbed. ";
        break;
    case "apple":
        blurb = "The phytonutrients in apples can help you regulate your blood sugar. ";
        break;
    case "broccoli":
        blurb = "Green vegetables (including some salad vegetables), beetroot, cauliflower, asparagus, dried peas, beans and lentils are a good source of folate";
        break;
    case "butter":
        blurb = "There are a lot of fat soluble vitamins in butter. This includes vitamins A, E and K2. ";
        break;
    case "cheese":
        blurb = "Milk, cheese and yoghurt provide calcium in a readily absorbable and convenient form. ";
        break;
    case "milk":
        blurb = "Milk, cheese and yoghurt provide calcium in a readily absorbable and convenient form. ";
        break;
    case "steak":
        blurb = "Lean red meats are a particularly good source of iron, zinc and B12 and are easily absorbed.";
        break;
    case "cereal":
        blurb = "Refined grains (for example, white flour), have had the bran and germ layers removed.  ";
        break;
    case "rice":
        blurb = "One of nature’s most perfect and versatile foods, rice is easy to digest and replete with important nutrients essential for good health.";
        break;
    case "raspberry":
        blurb = "The phytonutrients found in raspberries, especially rheosmin (also called raspberry ketone) have been found to increase enzyme activity, oxygen consumption, and heat production.";
        break;
    case "bread":
        blurb = "Bread is a source of proteins, vitamins, minerals, fibre and complex carbohydrates.";
        break;
    case "rye":
        blurb = "Reduce the risk of developing coronary heart disease, colon cancer, diabetes and diverticular disease. Maintain a healthy digestive system and prevent constipation. ";
        break;
    case "chocolate":
        blurb = "A discretionary food that has high levels of kilojoules, added sugars and should be eaten in moderation as it is associated with increased risk of obesity and chronic disease such as heart disease, stroke, type 2 diabetes";
        break;
    case "beer":
        blurb = "Beer is commonly made from a recipe of water, grain, hops, and yeast. Brewing is the process that converts the carbohydrate source into sugary liquid. Limit the consumption of alcohol due to the negative effects on the liver";
        break;
    case "wine":
        blurb = "Carbohydrates in wine come from sugar and alcohol. Wine contains 9 – 19 grams of carbs per serving. Although some data suggests that there may be health benefiuts to red wine.";
        break;
    case "pasta":
        blurb = "Refined grains, such as white flour are nearly always used in processed foods, such as cakes and biscuits. ";
        break;
    case "sauce":
        blurb = "Pasta sauce is a traditional Italian condiment, usually made with tomatoes and spices. Commercially prepared pasta sauces may differ in sodium and nutrient density.";
        break;
    case "carrot":
        blurb = "An exceptionally rich source of carotenes and vitamin-A. 100 g fresh carrot contains 8285 µg of beta-carotene and 16706 IU of vitamin A. ";
;        break;
    case "potato":
        blurb = "It’s a surprise for many to discover one medium potato (5.3 oz) with the skin contains: 45 percent of the daily value for vitamin C. ";
        break;
    case "salmon":
        blurb = "A rich source of phosphorous and potassium, with 139 milligrams and 149 milligrams, respectively, cold-smoked salmon still has more than half of the daily recommended intake of sodium for adults";
        break;
    case "banana":
        blurb = "The banana is the best fruit source of vitamin B6 (one medium banana provides about 25% of daily needs; one big banana provides about a third of daily needs).";
        break;
    case "biscuit":
        blurb = "A discretionary food that has high levels of kilojoules, added sugars and should be eaten in moderation as it is associated with increased risk of obesity and chronic disease such as heart disease, stroke, type 2 diabetes";
        break;

    }

return blurb;

}
