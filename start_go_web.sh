CURR=`pwd`
GOPATH=$CURR/server/go
export GOVHACK_WEB_PORT=8080
export GOVHACK_STATIC_WEB=$CURR/web
export MONGOHQ_URL=mongodb://localhost/nutri

# Rebuild binaries
rm -f $GOPATH/bin/*
go install govhack
$GOPATH/bin/govhack &

